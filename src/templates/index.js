import React, { Component } from 'react';
import Header from './header';
import Footer from './footer';
import '../App.css';
import Emailer from '../components/Emailer/Emailer';

/**
 * Main page que carrega cabeçalho, conteúdo, rodapé
 */

class Index extends Component {

    render() {
        return (
            <div>
                <Header />
                <div className="content-wrapper bgimg" style={{ minHeight: 656 }}>
                    <Emailer />
                </div>
                <Footer />
            </div>
        );
    }
}

export default Index;

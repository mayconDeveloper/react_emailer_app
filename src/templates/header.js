import React, { Component } from 'react';

export default class Header extends Component {

    render() {
        return (
            <header className="main-header">
                <a href="/" className="logo">
                    <i className="fa fa-inbox"></i>
                </a>
                <nav className="navbar navbar-static-top">
                    <div className="navbar-custom-menu">
                        <ul className="nav navbar-nav">
                        </ul>
                    </div>
                </nav>
            </header >
        )
    }
}
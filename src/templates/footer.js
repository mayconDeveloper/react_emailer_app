import React, { Component } from 'react';

export default class Footer extends Component {
    render() {
        return (
            <footer className="main-footer hidden-xs">
                <div className="pull-right">
                    <b>Versão: 0</b>
                </div>
                <strong>Copyright © {new Date().getFullYear()} <a href="https://www.emailer.com.br">Emailer</a>.</strong> Todos os direitos reservados.
            </footer>
        );
    }
}

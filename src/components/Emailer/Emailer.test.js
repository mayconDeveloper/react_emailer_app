import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Emailer from './Emailer';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("Emailer component", () => {
    test("renders", () => {
        const wrapper = shallow(<Emailer />);

        expect(wrapper.exists()).toBe(true);
    })
})
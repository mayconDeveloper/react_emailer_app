import React, { Component } from 'react';
import dados from '../../origens.json';

export default class Emailer extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            iconeSelection: "iconeSelection",
            showIcon: "",
            dados: dados
        }
    }

    random_bg_color() {
        var x = Math.floor(Math.random() * 100);
        var y = Math.floor(Math.random() * 256);
        var z = Math.floor(Math.random() * 256);
        var bgColor = "rgb(" + x + "," + y + "," + z + ")";

        return bgColor;
    }

    CheckLine(idEl) {
        if (document.getElementById("iconeChecked" + idEl + "").className !== "fa fa-check") {
            this.state.dados.filter(f => f.id === idEl)[0].checked = true;

            document.getElementById("iconeChecked" + idEl + "").className = "fa fa-check";
            this.setState({ iconeSelection: "selected", showIcon: "notShow" });
            document.getElementById("div" + idEl + "").className = "mainRow selectedRow";
        }
        else {
            this.state.dados.filter(f => f.id === idEl)[0].checked = false;

            document.getElementById("iconeChecked" + idEl + "").className = "";
            document.getElementById("div" + idEl + "").className = "mainRow";

            if (this.state.dados.filter((el) => el.checked === true).length === 0) {
                this.setState({ iconeSelection: "iconeSelection", showIcon: "" });
            }
        }
    }

    componentWillMount() {
        dados.map((el) => el.colorIcon = this.random_bg_color());
    }

    render() {

        return (
            <div className="container">
                {dados.map((el) =>
                    <div className="row" key={el.id}>
                        <div className="col-md-12">
                            <div className="mainRow" id={"div" + el.id}>
                                <div className="divIcon">
                                    <button className={this.state.iconeSelection} onClick={() => this.CheckLine(el.id)} style={{ backgroundColor: el.colorIcon }}>
                                        <i id={"iconeUnchecked" + el.id} className={this.state.showIcon}>{el.initials}</i>
                                        <i id={"iconeChecked" + el.id}></i>
                                    </button>
                                </div>
                                <div>
                                    <div>
                                        <span>{el.Title}</span>
                                    </div>
                                    <div>
                                        <span>{el.description}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
                }
            </div>
        );
    }
}
